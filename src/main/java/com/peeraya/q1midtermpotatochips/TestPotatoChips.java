/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.q1midtermpotatochips;

/**
 *
 * @author Administrator
 */
public class TestPotatoChips {
    public static void main(String[] args) {
        PotatoChips pc = new PotatoChips("Potato chips", 0, 0, 0);
        pc.showDetails();
        pc.BreakRange();
        
        PotatoChips lays = new PotatoChips("Lay's", 20, 48, 260);
        lays.showDetails();
        lays.BreakRange();
        
        PotatoChips tasto = new PotatoChips("Tasto", 20, 50, 260);
        tasto.showDetails();
        tasto.BreakRange();
        
        PotatoChips karamucho = new PotatoChips("Karamucho", 24, 48, 260);
        karamucho.showDetails();
        karamucho.BreakRange();
    }
}
