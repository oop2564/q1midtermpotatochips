/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.q1midtermpotatochips;

/**
 *
 * @author Administrator
 */
public class PotatoChips {

    private String name;
    private int price;
    private int NW;
    private int TE;
    
    public PotatoChips(String name, int price, int NW, int TE) {
        System.out.println("Potato chips created");
        this.name = name;
        this.price = price;
        this.NW = NW;
        this.TE = TE;
    }
    
    public void showDetails() {
        System.out.println(this.name + " created");
        System.out.println("Price : " + this.price + " baht");
        System.out.println("Net Weight : " + this.NW + " g.");
        System.out.println("Total Energy : " + this.TE + " kcal");
    }
    
    public void BreakRange() {
        System.out.println("-------------------------");
    }
}
